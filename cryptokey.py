# http://lira.epac.to:8080/doc//python-crypto-doc/
from Crypto.Random import random
from Crypto.PublicKey import DSA
from Crypto.Hash import SHA256


class CryptoKey:
    def __init__(self):
        self.key = DSA.generate(1024)

    def getPrivateKey(self):
        return self.key

    def getPublicKey(self):
        return self.key.publickey

    def signMessage(self, message):
        h = SHA256.new(message).digest()
        k = random.StrongRandom().randint(1, self.key.q-1)
        signature = self.key.sign(h, k)
        return signature

    def verifySignature(self, signature, expectedMessage):
        h = SHA256.new(expectedMessage).digest()
        if self.key.verify(h, signature):
            return True
        else:
            return False
