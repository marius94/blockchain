## How to install:
Run the following command to install required modules
```pip install -r requirements.txt```

## Running the code
Run the main file to try it out
```python main.py```

## Testing
When pushing to remote master, all tests in tests.py will be run automatically. Please make sure it is passing before pushing
