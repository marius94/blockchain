#!/usr/bin/python

from block import Block
from blockchain import BlockChain
from transaction import Transaction
from user import User


# Initiate blockchain
blockChain = BlockChain()

# Create users for that blockchain
user1 = User(blockChain)
user2 = User(blockChain)

# Add blocks
user1.addBlockToChain("Block 1")
user1.addBlockToChain("Block 2")
user2.addBlockToChain("Block 1")
user1.addBlockToChain("Block 3")
user2.addBlockToChain("Block 2")

# Print content
print "\nAll blocks in chain:\n"
blockChain.printChain()

user1.getMyBlocks()
user2.getMyBlocks()
print "\n\nUser 1 blocks:\n"
[x.printBlock() for x in user1.myBlocks]
print "\n\nUser 2 blocks:\n"
[x.printBlock() for x in user2.myBlocks]

blockChain.isChainValid()
blockChain.tamperWithBlock(3)
blockChain.isChainValid()

