import datetime
import hashlib

class Block:
    previousHash = "0"
    data = ""
    timeOfCreation = ""
    hashValue = ""
    signature = ""

    def __init__(self, data, previousBlock = "0"):
        self.data = data
        self.timeOfCreation = datetime.datetime.now().strftime("%Y-%m-%d %X")

        if(previousBlock != "0"):
            self.previousHash = previousBlock.getHash()


    def getHash(self):
        stringToHash = str(self.previousHash) + self.data + self.timeOfCreation
        return hashlib.sha256(stringToHash).hexdigest()

    def printBlock(self):
        print "Previous hash: " + str(self.previousHash)
        print "Data: " + self.data
        print "Time of creation: " + self.timeOfCreation
        print "Hash value: " + self.getHash()
        print "\n"

    def addSignature(self, signature):
        self.signature = signature
