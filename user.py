from cryptokey import CryptoKey
from blockchain import BlockChain

class User:

    def __init__(self, blockChain):
        self.crypto = CryptoKey()
        self.myBlocks = []
        self.blockChain = blockChain

    def identifyBlockAsMine(self, block):
        expectedMessage = block.getHash()
        if self.crypto.verifySignature(block.signature, expectedMessage):
            return True
        else:
            return False

    def getMyBlocks(self):
        self.myBlocks = []
        for block in self.blockChain.blocksInChain:
            if self.identifyBlockAsMine(block):
                self.myBlocks.append(block)

    def signBlock(self, block):
        messageToSign = block.getHash()
        signature = self.crypto.signMessage(messageToSign)
        block.addSignature(signature)

    def addBlockToChain(self, data):
        self.blockChain.addBlock(data, self)

    def printAllMyBlocks(self):
        self.getMyBlocks()
        [block.printBlock() for block in self.myBlocks]

    def countAllMyBlocks(self):
        self.getMyBlocks()
        return len(self.myBlocks)
