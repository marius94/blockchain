from block import Block

class BlockChain:

    def __init__(self):
        self.blocksInChain = []

    def addBlock(self, data, user):
        if not self.blocksInChain:
            newBlock = Block(data) # genesis block (first block)
        else:
            newBlock = Block(data, self.blocksInChain[-1]) # get last element in list of blocks

        user.signBlock(newBlock)
        self.blocksInChain.append(newBlock)

    def printChain(self):
        for block in self.blocksInChain:
            block.printBlock()

    def countAllBlocks(self):
        return len(self.blocksInChain)

    def isChainValid(self):
        previousBlockHash = "0"
        for block in self.blocksInChain:
            currentBlockHash = block.getHash()
            if not (block.getHash() == currentBlockHash):
                # print "Invalid hash value!"
                # print block.hashValue + " is not equal to " + currentBlockHash
                return False

            if not (block.previousHash == previousBlockHash):
                # print "Invalid previous hash!"
                # print block.previousHash + " is not equal to " + previousBlockHash
                return False

            previousBlockHash = currentBlockHash
        return True

    # For testing purposes
    def tamperWithBlock(self, indexOfBlock):
        blockToTamperWith = self.blocksInChain[indexOfBlock]
        blockToTamperWith.data += "appending gargage"
