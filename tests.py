import unittest
from blockchain import BlockChain
from user import User
from cryptokey import CryptoKey


class TestAsymmetricEncryption(unittest.TestCase):
    def test_validateMessage(self):
        crypto = CryptoKey()
        authenticHash = "Hash of original message"
        signature = crypto.signMessage(authenticHash)
        expectedHash = authenticHash
        isValidSignature = crypto.verifySignature(signature, expectedHash)

        assert(isValidSignature == True)

        expectedHash = "Hash from modified message"
        isValidSignature = crypto.verifySignature(signature, expectedHash)

        assert(isValidSignature == False)

class TestTamperWithBlock(unittest.TestCase):
    def test_tamperWithBlock(self):
        blockChain = BlockChain()
        user = User(blockChain)

        for counter in range(15):
            user.addBlockToChain("Block number " + str(counter))

        assert(blockChain.isChainValid() == True)
        blockChain.tamperWithBlock(7)
        assert(blockChain.isChainValid() == False)

class TestAddBlocks(unittest.TestCase):
    def test_oneUser(self):
        blockChain = BlockChain()
        user = User(blockChain)

        for counter in range(15):
            user.addBlockToChain("Block number " + str(counter))

        assert(blockChain.countAllBlocks() == 15)

    def test_multipleUsers(self):
        blockChain = BlockChain()
        user1 = User(blockChain)
        user2 = User(blockChain)
        user3 = User(blockChain)

        for counter in range(3):
            user1.addBlockToChain("Block number " + str(counter))

        user2.addBlockToChain("Block number 0")
        user1.addBlockToChain("Block number 3")
        user3.addBlockToChain("Block number 0")
        user3.addBlockToChain("Block number 1")

        assert(blockChain.countAllBlocks() == 7)

        assert(user1.countAllMyBlocks() == 4)
        assert(user2.countAllMyBlocks() == 1)
        assert(user3.countAllMyBlocks() == 2)


if __name__ == '__main__':
    unittest.main()
